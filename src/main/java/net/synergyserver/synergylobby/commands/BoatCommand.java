package net.synergyserver.synergylobby.commands;

import net.synergyserver.synergycore.commands.CommandDeclaration;
import net.synergyserver.synergycore.commands.CommandFlags;
import net.synergyserver.synergycore.commands.MainCommand;
import net.synergyserver.synergycore.commands.SenderType;
import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "boat",
        permission = "syn.boat",
        usage = "/boat",
        description = "RIP /boat. November 2014 - July 2017.",
        maxArgs = 0,
        validSenders = SenderType.PLAYER
)
public class BoatCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        sender.sendMessage("RIP /boat. November 2014 - July 2017");

//        Player player = (Player) sender;
//        MinecraftProfile mcp = PlayerUtil.getProfile(player);
//
//        // If the current WorldGroupProfile isn't a LobbyWorldGroupProfile then give an error
//        if (!(mcp.getCurrentWorldGroupProfile() instanceof LobbyWorldGroupProfile)) {
//            player.sendMessage(Message.get("commands.boat.error.not_in_lobby"));
//            return false;
//        }
//
//        // If they're already in a vehicle then give an error
//        if (player.isInsideVehicle()) {
//            player.sendMessage(Message.get("commands.boat.error.already_in_boat"));
//            return false;
//        }
//
//        LobbyWorldGroupProfile wgp = (LobbyWorldGroupProfile) mcp.getCurrentWorldGroupProfile();
//
//        // Create the location of the starting point
//        World world = Bukkit.getWorld(PluginConfig.getConfig(SynergyLobby.getPlugin()).getString("boating.start.world"));
//        double x = PluginConfig.getConfig(SynergyLobby.getPlugin()).getDouble("boating.start.x");
//        double y = PluginConfig.getConfig(SynergyLobby.getPlugin()).getDouble("boating.start.y");
//        double z = PluginConfig.getConfig(SynergyLobby.getPlugin()).getDouble("boating.start.z");
//        float yaw = Float.parseFloat(PluginConfig.getConfig(SynergyLobby.getPlugin()).getString("boating.start.yaw"));
//        float pitch = Float.parseFloat(PluginConfig.getConfig(SynergyLobby.getPlugin()).getString("boating.start.pitch"));
//        Location startLocation = new Location(world, x, y, z, yaw, pitch);
//
//        // Teleport the player and put them in a boat
//        player.teleport(startLocation);
//        world.spawnEntity(startLocation, EntityType.BOAT).setPassenger(player);
//
//        // Start the timer
//        wgp.setBoatStart(System.currentTimeMillis());
        return true;
    }
}
