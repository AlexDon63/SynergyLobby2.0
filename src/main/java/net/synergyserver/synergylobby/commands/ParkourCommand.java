package net.synergyserver.synergylobby.commands;

import net.synergyserver.synergycore.commands.CommandDeclaration;
import net.synergyserver.synergycore.commands.CommandFlags;
import net.synergyserver.synergycore.commands.MainCommand;
import net.synergyserver.synergycore.commands.SenderType;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.configs.PluginConfig;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergylobby.SynergyLobby;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;

@CommandDeclaration(
        commandName = "parkour",
        permission = "syn.parkour",
        usage = "/parkour",
        description = "Teleports you to the start of the ladder parkour in Lobby.",
        maxArgs = 0,
        validSenders = SenderType.PLAYER
)
public class ParkourCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        Location parkourStart = BukkitUtil.locationFromString(PluginConfig.getConfig(SynergyLobby.getPlugin()).getString("parkour"));

        // Teleport the player and give them feedback
        boolean success = player.teleport(parkourStart, PlayerTeleportEvent.TeleportCause.COMMAND);
        if (success) {
            player.sendMessage(Message.get("commands.parkour.game_info.teleported"));
        }
        return true;
    }
}
