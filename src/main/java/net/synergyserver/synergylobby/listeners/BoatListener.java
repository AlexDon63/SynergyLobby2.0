package net.synergyserver.synergylobby.listeners;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.configs.PluginConfig;
import net.synergyserver.synergycore.database.MongoDB;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergycore.utils.TimeUtil;
import net.synergyserver.synergylobby.BoatRecord;
import net.synergyserver.synergylobby.LobbyWorldGroupProfile;
import net.synergyserver.synergylobby.SynergyLobby;
import org.bukkit.Bukkit;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.block.Skull;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Boat;
import org.bukkit.entity.Player;
import org.bukkit.entity.Vehicle;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.vehicle.VehicleDestroyEvent;
import org.bukkit.event.vehicle.VehicleExitEvent;
import org.mongodb.morphia.Datastore;

import java.util.List;

/**
 * Listens to events that have to do with the boat minigame.
 */
public class BoatListener implements Listener {

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBoatExit(VehicleExitEvent event) {
        Vehicle vehicle = event.getVehicle();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyLobby.getWorldGroup().contains(vehicle.getWorld().getName())) {
            return;
        }

        // Ignore the event if it's not a boat
        if (!(vehicle instanceof Boat)) {
            return;
        }

        // Ignore the event if it isn't a player that is exiting the boat
        if (!(event.getExited() instanceof Player)) {
            return;
        }

        Player player = (Player) event.getExited();
        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        // Ignore the event if, for whatever reason, the current WGP is not a lobby WGP
        if (!(mcp.getCurrentWorldGroupProfile() instanceof LobbyWorldGroupProfile)) {
            return;
        }

        LobbyWorldGroupProfile wgp = (LobbyWorldGroupProfile) mcp.getCurrentWorldGroupProfile();

        // Ignore the event if the player isn't boating
        if (!wgp.isBoating()) {
            return;
        }

        // Stop the timer, remove the boat, and teleport the player out of the way
        wgp.setBoatEnd(System.currentTimeMillis());
        vehicle.remove();
        Bukkit.getScheduler().runTaskLater(SynergyLobby.getPlugin(), new Runnable() {
            @Override
            public void run() {
                player.teleport(player.getWorld().getSpawnLocation());
            }
        }, 1L);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyLobby.getWorldGroup().contains(player.getWorld().getName())) {
            return;
        }

        // Ignore the event if the player isn't riding a boat
        if (!player.isInsideVehicle()) {
            return;
        }
        Vehicle vehicle = (Vehicle) player.getVehicle();
        if (!(vehicle instanceof Boat)) {
            return;
        }

        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        // Ignore the event if, for whatever reason, the current WGP is not a lobby WGP
        if (!(mcp.getCurrentWorldGroupProfile() instanceof LobbyWorldGroupProfile)) {
            return;
        }

        LobbyWorldGroupProfile wgp = (LobbyWorldGroupProfile) mcp.getCurrentWorldGroupProfile();

        // Ignore the event if the player isn't boating
        if (!wgp.isBoating()) {
            return;
        }

        // Otherwise, stop the timer, remove the boat, and teleport the player out of the way
        wgp.setBoatEnd(System.currentTimeMillis());
        vehicle.remove();
        Bukkit.getScheduler().runTaskLater(SynergyLobby.getPlugin(), new Runnable() {
            @Override
            public void run() {
                player.teleport(player.getWorld().getSpawnLocation());
            }
        }, 1L);
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onBoatBreak(VehicleDestroyEvent event) {
        Vehicle vehicle = event.getVehicle();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyLobby.getWorldGroup().contains(vehicle.getWorld().getName())) {
            return;
        }

        // Ignore the event if it's not a boat
        if (!(vehicle instanceof Boat)) {
            return;
        }

        // Ignore the event if the boat doesn't have a passenger
        if (vehicle.getPassenger() == null || !(vehicle.getPassenger() instanceof Player)) {
            return;
        }

        Player player = (Player) vehicle.getPassenger();
        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        // Ignore the event if, for whatever reason, the current WGP is not a lobby WGP
        if (!(mcp.getCurrentWorldGroupProfile() instanceof LobbyWorldGroupProfile)) {
            return;
        }

        LobbyWorldGroupProfile wgp = (LobbyWorldGroupProfile) mcp.getCurrentWorldGroupProfile();

        // Ignore the event if the player isn't boating
        if (!wgp.isBoating()) {
            return;
        }

        // Otherwise, cancel the event
        event.setCancelled(true);

        // If the boat didn't break because of crashing, exit this method
        if (event.getAttacker() != null) {
            return;
        }

        // Otherwise, stop the timer, remove the boat, and teleport the player out of the way
        wgp.setBoatEnd(System.currentTimeMillis());
        vehicle.remove();
        Bukkit.getScheduler().runTaskLater(SynergyLobby.getPlugin(), new Runnable() {
            @Override
            public void run() {
                player.teleport(player.getWorld().getSpawnLocation());
            }
        }, 1L);
    }


    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBoatFinish(PlayerMoveEvent event) {
        Player player = event.getPlayer();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyLobby.getWorldGroup().contains(player.getWorld().getName())) {
            return;
        }

        // Ignore the event if the player isn't riding a boat
        if (!player.isInsideVehicle()) {
            return;
        }
        Vehicle vehicle = (Vehicle) player.getVehicle();
        if (!(vehicle instanceof Boat)) {
            return;
        }

        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        // Ignore the event if, for whatever reason, the current WGP is not a lobby WGP
        if (!(mcp.getCurrentWorldGroupProfile() instanceof LobbyWorldGroupProfile)) {
            return;
        }

        LobbyWorldGroupProfile wgp = (LobbyWorldGroupProfile) mcp.getCurrentWorldGroupProfile();

        // Ignore the event if the player isn't boating
        if (!wgp.isBoating()) {
            return;
        }

        // Ignore the event if they haven't passed the finish yet
        if (player.getLocation().getX() < 57) {
            return;
        }

        // Remove the boat and create a BoatRecord
        wgp.setBoatEnd(System.currentTimeMillis());
        vehicle.remove();

        long time = wgp.getBoatEnd() - wgp.getBoatStart();
        BoatRecord boatRecord = new BoatRecord(player.getUniqueId(), System.currentTimeMillis(), time);
        String timeMessage = Message.getTimeMessage(time, 2, 1);

        // Check if the record is even good, by comparing it to the player's previous records first
        boolean wasAdded = wgp.addBoatRecord(boatRecord);

        // If the record isn't worthy then send the player a message
        if (!wasAdded) {
            player.sendMessage(Message.format("boating.info.finish.regular", timeMessage));
            return;
        }

        // Get the top three records
        Datastore ds = MongoDB.getInstance().getDatastore();
        List<BoatRecord> topRecords = ds.createQuery(BoatRecord.class).order("t").limit(3).asList();

        YamlConfiguration config = PluginConfig.getConfig(SynergyLobby.getPlugin());

        // Update the leaderboards
        int place = Integer.MAX_VALUE;
        for (int i = 0; i < topRecords.size(); i++) {
            BoatRecord currentRecord = topRecords.get(i);

            // If the player's record is one of the top three, save the index
            if (time == currentRecord.getTime()) {
                place = i + 1;
            }

            String configNode = "boating.leaderboard." + (i + 1) + ".";

            // Update the leaderboard blocks
            String placeShort = config.getString(configNode + "string_short");
            long seconds = TimeUtil.toFlooredUnit(TimeUtil.TimeUnit.MILLI, TimeUtil.TimeUnit.SECOND, currentRecord.getTime());
            double ticks = TimeUtil.toRoundedUnitRemainder(TimeUtil.TimeUnit.MILLI, TimeUtil.TimeUnit.GAME_TICK, currentRecord.getTime(), 1);

            int signX = config.getInt(configNode + "sign.x");
            int signY = config.getInt(configNode + "sign.y");
            int signZ = config.getInt(configNode + "sign.z");
            int skullX = config.getInt(configNode + "skull.x");
            int skullY = config.getInt(configNode + "skull.y");
            int skullZ = config.getInt(configNode + "skull.z");

            // Update the sign
            BlockState signState = player.getWorld().getBlockAt(signX, signY, signZ).getState();
            if (signState instanceof Sign) {
                Sign sign = (Sign) signState;
                sign.setLine(0, Message.format("boating.leaderboard.sign_row_1", placeShort));
                sign.setLine(1, Message.format("boating.leaderboard.sign_row_2", PlayerUtil.getName(currentRecord.getPlayerID())));
                sign.setLine(2, Message.format("boating.leaderboard.sign_row_3", seconds, ticks));
                sign.update();
            }

            // Update the skull
            BlockState skullState = player.getWorld().getBlockAt(skullX, skullY, skullZ).getState();
            if (skullState instanceof Skull) {
                Skull skull = (Skull) skullState;
                skull.setOwningPlayer(Bukkit.getOfflinePlayer(currentRecord.getPlayerID()));
                skull.update();
            }
        }

        // If they didn't get on the leaderboard but got in their own top 5
        if (place > 3) {
            // If they got a new PB send them a special message
            if (boatRecord.getTime() == wgp.getBestBoatRecord().getTime()) {
                player.sendMessage(Message.format("boating.info.finish.personal_best", timeMessage));
                return;
            }
            // Otherwise just send them a regular message
            player.sendMessage(Message.format("boating.info.finish.regular", timeMessage));
            return;
        }

        // Send them a congratulatory message for getting into the top 3
        String placeWord = config.getString("boating.leaderboard." + place + ".string");
        player.sendMessage(Message.format("boating.info.finish.leaderboard", placeWord, timeMessage));
    }
}
