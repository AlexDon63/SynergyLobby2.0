package net.synergyserver.synergylobby.listeners;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergylobby.LobbyWorldGroupProfile;
import net.synergyserver.synergylobby.SynergyLobby;
import net.synergyserver.synergylobby.TheHunt;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;

/**
 * Listens to events related to The Hunt minigame.
 */
public class TheHuntListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onClaimEgg(PlayerInteractEvent event) {
        Player player = event.getPlayer();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyLobby.getWorldGroup().contains(player.getWorld().getName())) {
            return;
        }

        // Ignore the event if it wasn't a right click
        if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            return;
        }
        // Ignore the event created by the off hand to avoid double event calls
        if (event.getHand().equals(EquipmentSlot.OFF_HAND)) {
            return;
        }

        Block block = event.getClickedBlock();

        // Ignore the event if it isn't a skull that was clicked
        if (!block.getType().equals(Material.PLAYER_HEAD)) {
            return;
        }

        TheHunt theHunt = TheHunt.getInstance();
        String eggID = theHunt.getEggID(block.getLocation());

        // If an egg wasn't at the location then ignore the event
        if (eggID == null) {
            return;
        }

        // If the user is flying then call them a cheater
        if (player.isFlying()) {
            player.sendMessage(Message.format("the_hunt.error.cheating"));
            event.setUseItemInHand(Event.Result.DENY);
            event.setCancelled(true);
            return;
        }

        WorldGroupProfile wgp = PlayerUtil.getProfile(player).getCurrentWorldGroupProfile();

        // Ignore the event if their current wgp isn't a Lobby wgp because that means something went wrong
        if (!(wgp instanceof LobbyWorldGroupProfile)) {
            return;
        }

        LobbyWorldGroupProfile lwgp = (LobbyWorldGroupProfile) wgp;

        // If they already found this egg then tell them
        if (lwgp.hasFoundEgg(eggID)) {
            player.sendMessage(Message.get("the_hunt.game_info.egg_already_found"));
        }
        // Otherwise add the egg to their collection and give them feedback
        else {
            lwgp.addFoundEgg(eggID);
            player.sendMessage(Message.format("the_hunt.game_info.found_new_egg", lwgp.getFoundEggs().size(), theHunt.getEggCount()));
        }

        // Cancel the event so it doesn't give skull owner feedback
        event.setUseItemInHand(Event.Result.DENY);
        event.setCancelled(true);
    }

}
